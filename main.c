//
//  main.c
//  SuperMarket
//
//  Created by Johan Persson on 2013-11-14.
//  Copyright (c) 2013 Johan Persson. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "queue.h"


#define CUSTOMER_EVERY_X_SECOND 60
#define HANDLE_ITEM_TIME 8
#define PAYMENT_TIME 20
#define NBR_OF_REGISTERS 10

static Customer *create_customer(int time) {
	Customer *noderef = NULL;
	noderef = malloc(sizeof(struct Customer));
	int rand_nbr = (rand() % (100 - 0 + 1)) + 0;
	noderef->nbr_of_items = rand_nbr;
	noderef->time_to_pay = time+PAYMENT_TIME+(rand_nbr*HANDLE_ITEM_TIME);
	return noderef;
}

int dequeue(Customer *data) {
	Customer *first_node;
	int result = 0;
	first_node = lineref.first;
	if (lineref.first != NULL) {
		lineref.first = first_node->next;
		//*data = destroy_node(first_node);
		result = 1;
		if (lineref.first == NULL) {
			lineref.last = NULL;
		}
	}
	return result;
}

void add_customer_to_line(Customer *new_customer) {
	Customer *last_customer;
	lineref.nbr_customers_in_line++;
	
	if (new_customer != NULL) {
		last_customer = lineref.last;
	}
	if (lineref.first == NULL){
		lineref.first = new_customer;
		lineref.last = new_customer;
	}
	else
	{
		last_customer->next = new_customer;
		lineref.last = new_customer;
	}
}

void swap(Cash_register_line arr[], int i, int j) {
    Cash_register_line temp = arr[j];
    arr[j] = arr[i];
    arr[i] = temp;
}

void QuickSortHeltal(Cash_register_line array[], int first, int last)
{
	int low = first;
	int high = last;
	//int x = array[(first+last)/2].get();
	int x = array[(first/last)/2].customers_in_line;
	do {
		while (array[low].customers_in_line<x) {
			low++;
		}
		while (array[high].customers_in_line> x) {
			high--;
		}
		if (low<=high) {
			swap(array,low,high);
			low++;
			high--;
		}
	} while (low<=high);
	
	if(first < high)
		QuickSortHeltal(array,first,high);
	if(low < last)
		QuickSortHeltal(array,low,last);
}

//int enqueue(Customer customer) {
//	Customer *new_node, *last_node;
//	int result = 0;
//	new_node = create_node(data);
//	if (new_node != NULL) {
//		last_node = queueref.last;
//	}
//	if (queueref.first == NULL){
//		queueref.first = new_node;
//		queueref.last = new_node;
//		result = 1;
//	}
//	else
//	{
//		last_node->next = new_node;
//		queueref.last = new_node;
//		result = 1;
//	}
//	return result;
//}

void print_queue() {
	Customer *curr_ptr = lineref.first;
	printf("\n");
	if (curr_ptr == NULL) {
		printf("%s","Queue Empty");
	}
	while (curr_ptr != NULL) {
		printf("%d\n",curr_ptr->nbr_of_items);
		curr_ptr = curr_ptr->next;
	}
	printf("\n Line 1: %d\n", lineref.nbr_customers_in_line);
	
}



int main(int argc, const char * argv[])
{
	int cust = 0;
	int run_time_h = 2;
	int run_time_s = run_time_h *60*60;
	int current_time_s = 0;
   	Cash_register_line lines[3];
	Cash_register_line linje;
	double low = 0, middle = 0, high= 0, none = 0;
	srand(time(NULL));
	int values[] = { 88, 56, 100, 2, 25 };
	//QuickSortHeltal(lines, 0, 2);
	//qsort(lines, 3, sizeof(int), cmpfunc);
	for(int n = 0 ; n < 3; n++ ) {
		printf("%d ", lines[n].customers_in_line);
	}
	do {
		
		// OPEN CASH REGISTER
		// If no registers are open open on
		// If any register has more than 6 in line open a new register
		// OBS! Max 10 registers
		
		
		//Cash_register_line line;
		
//		Customer customer;
//		customer.nbr_of_items = 10;
//		customer.time_to_pay = 100;
//		line.customers_in_line++;
//		//line.customer_array[0] = customer;
//		line.customers_in_line++;
//		lines[0] = line;
		//lines[1] =
		
		// GET SHORTEST LINE

		
		// ADD CUSTOMERS EVERY MINUTE
		// Use rand to see how many customers to add
		// Add customer to shortest open cash_registers line

		// IF A MINUTE HAS PASSED - ADD A CUSTOMER TO LINE
		if (current_time_s % 60 == 0) {
			Customer *new_customer, *last_customer;
			
			// RAND TO CREATE POS
			double rand_generator = (rand()/(double)(RAND_MAX + 1))+1;
			// ADD 1 CUSTOMER
			if ((rand_generator > 0.1) && (rand_generator <= 0.4)) {
				for (int i = 0; i < 2; i++) {
					low++;
					new_customer = create_customer(current_time_s);
					add_customer_to_line(new_customer);
//					//lineref.nbr_customers_in_line++;
//					if (new_customer != NULL) {
//						last_customer = lineref.last;
//					}
//					if (lineref.first == NULL){
//						lineref.first = new_customer;
//						lineref.last = new_customer;
//					}
//					else
//					{
//						last_customer->next = new_customer;
//						lineref.last = new_customer;
//					}
				}
			}
			// ADD 2 CUSTOMERS
			else if ((rand_generator > 0.4) && (rand_generator <= 0.9)) {
				for (int i = 0; i < 2; i++) {
					middle++;
					new_customer = create_customer(current_time_s);
					add_customer_to_line(new_customer);
					//lineref.nbr_customers_in_line++;
				}
			}
			
			// ADD 3 CUSTOMERS
			else if ((rand_generator > 0.9) && (rand_generator <= 1.0)) {
				for (int i = 0; i < 2; i++) {
					high++;
					new_customer = create_customer(current_time_s);
					add_customer_to_line(new_customer);
					//lineref.nbr_customers_in_line++;
				}
			}
			// DON'T ADD A CUSTOMER
			else {
				none++;
				lineref.nbr_customers_in_line++;
			}
		}
		
		
		// SEE IF CUSTOMERS ARE LEAVING
		// GO THROUGH EVERY LINE AND CHECK FIRST POS
		if (lineref.first != NULL) {
			if (lineref.first->time_to_pay < current_time_s) {
				Customer *delete_customer;
				delete_customer = lineref.first;
				dequeue(delete_customer);
				lineref.nbr_customers_in_line--;
				printf("ta bort\n");
			}
		}
		// CLOSE REGISTER
		// If cash_register line = 0 close all registers but one
		
		current_time_s++;
	} while (current_time_s < 1080);
	printf("Low: %f Middle: %f High: %f None: %f", low, middle, high, none);
	//printf("%d", lineref.first);
	print_queue();
	return 0;
}



