//
//  queue.h
//  SuperMarket
//
//  Created by Johan Persson on 2013-12-16.
//  Copyright (c) 2013 Johan Persson. All rights reserved.
//

#ifndef SuperMarket_queue_h
#define SuperMarket_queue_h
#define QUEUE_MAX_SIZE 10
#define MAX_REGISTERS 10

typedef struct {
	struct Customer *first;
	struct Customer *last;
	int nbr_customers_in_line;
} line;

static line lineref;


typedef struct Customer{
	int nbr_of_items; //Random number 1-50
	int time_to_pay; // 8sek handling * nbr items + 20sek to pay
	struct Customer *next;
}Customer;

typedef struct Cash_register_line{
	int customers_in_line; //Nbr customers in line
	//Customer customer_array[QUEUE_MAX_SIZE];	// Array of customers in line
	Customer *first;
	Customer *last;
	int head;
	int tail;
}Cash_register_line;

static Cash_register_line line_ref;


#endif